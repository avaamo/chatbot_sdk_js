"use strict";

function BotMessage(context) {
    this.messages = [];
    this.context = context;
}

BotMessage.prototype.addTextResponse = function(message) {
    if (!message)
        throw "Message cannot be empty";
    this.messages.push(message);
};

BotMessage.prototype.addCardResponse = function(card) {
    if (!card)
        throw "Message cannot be empty";
    if (!card.title && !card.description && !card.links && !card.inputs && !card.showcase_image)
        throw "Please use valid card syntax";
    this.messages.push({"card":card});
};

BotMessage.prototype.addCarouselResponse = function(carousel) {
    if (!carousel)
        throw "Carousel cannot be empty";
    this.messages.push(carousel);
};

BotMessage.prototype.getFormattedMessage = function() {
    console.log(this.context.getUpdatedContext());
    return {
        "messages": this.messages,
        "collectedVariables": this.context.getUpdatedContext(),
        "user": this.context.user(),
        "conversation": this.context.conversation()
    };
};

function BotMessageFactory(context) {

    if (context && !(context.constructor.name === "Context")) {
        throw "Do not overwrite context. Use the context in the callback.";
    }
    return new BotMessage(context);
}

module.exports = BotMessageFactory;
