"use strict";
const
    Node = require("./conversation_node"),
    WebSocket = require("ws"),
    Utils = require("./utils"),
    Context = require("./context"),
    Promise = require("promise"),
    Logger = Utils.Logger,
    Helper = Utils.Helper,
    BotMessage = require("./../lib/bot_message"),
    Intent = require("./../lib/intent");

function Avaamo(bot_uuid, bot_access_token, messageCallback) {
    this.bot_uuid = bot_uuid;
    this.bot_access_token = bot_access_token;
    this.url = Helper.DS_HOST + Helper.DS_URI(this.bot_access_token);
    this.isClosing = false;
    this.pingTimer = [];
    this.ref = 1;
    this.messages_channel = "bot_messages." + bot_uuid;
    this.server_channel = "server.chatbot_messages";
    this.channels = [this.messages_channel, this.server_channel];
    console.log("Bot listening...");
    console.log(this.url);
    this.socket = new WebSocket(this.url);
    this.join();
    this.onMessageCallback = messageCallback;
}

Avaamo.prototype.ping = function ping() {
    Logger.log("Pinging server...");
    let data = {
        topic: "phoenix",
        event: "heartbeat",
        payload: {},
        ref: this.ref++
    };
    this.socket.send(JSON.stringify(data), function pingSend(error) {
        if (error) {
            Logger.log("Error in sending ping message", error);
        }
    });
    this.pingTimer.push(setTimeout(function() {
        this.ping();
    }.bind(this), 30000));
};

Avaamo.prototype.join = function join() {
    let joinRef = this.ref;
    this.socket.on("open", (function open() {
        Logger.log("Connection successfully opened");
        //join channel
        this.channels.forEach(function(channel, key) {
            Logger.log(`Joining ${key} channel`);
            let data = {
                topic: channel,
                event: "phx_join",
                payload: {},
                ref: this.ref++
            };
            this.socket.send(JSON.stringify(data), function(error) {
                if (error) {
                    Logger.log("Join failed", error);
                } else {
                    Logger.log("Join request sent successfully");
                }
            });
        }.bind(this));
    }.bind(this)));
    this.socket.on("error", function error(error) {
        Logger.log("Error in socket", error);
    });
    this.socket.on("message", (event) => {
        let payload = JSON.parse(event);
        if (payload.topic === this.messages_channel && payload.event === "phx_reply" && payload.ref === joinRef) {
            if (payload.payload.status === "ok") {
                Logger.log("Joined messages channel successfully");
                this.ping();
            } else {
                Logger.log("Unable to join messages channel");
            }
        }
        if (payload.topic === this.messages_channel && payload.event === "message") {
            if (payload.payload.bot_message) {
                this.onMessageCallback(payload);
            } else {
                console.warn(payload);
            }
        }
    });
    this.socket.on("close", (event) => {
        Logger.log("Socket closed", event);
        if (this.isClosing !== true) {
            this.init(Logger.debug);
        }
    });
};

Avaamo.prototype.send = function(message) {
    let data = {
        topic: this.server_channel,
        event: "message",
        payload: {
            "bot_message": message
        },
        ref: this.ref++
    };
    this.socket.send(JSON.stringify(data), function(error) {
        if (error) {
            Logger.log("Sending failed", error);
        } else {
            Logger.log(`Message ${JSON.stringify(data)} sent successfully`);
        }
    });
};

function ChatBot(bot_uuid, bot_access_token) {

    if (!bot_uuid || !bot_access_token) {
        throw "bot_uuid and bot_access_token should not be null.";
    }

    this.bot_uuid = bot_uuid;
    this.bot_access_token = bot_access_token;
    this.dialog = null;
    this.init(true);
    this.bot_default_response = [];
    this.live_agent = {
        "enable": true
    };
}

ChatBot.prototype.init = function init(logger) {
    Logger.debug = logger;
    let onMessageCallback = (payload) => {
        this.onMessage(payload, this.dialog);
    };
    this.avaamo = new Avaamo(this.bot_uuid, this.bot_access_token, onMessageCallback);
};

ChatBot.prototype.onMessage = function(payload) {

    processIncomingMessage(payload.payload.bot_message, this.dialog).then((bot_message) => {
        if (bot_message) {
            if (!(bot_message.constructor.name === "BotMessage")) {
                throw "Please use bot_message to construct replies.";
            } else {
                let bot_message_formatted = bot_message.getFormattedMessage();
                bot_message_formatted.bot_uuid = this.bot_uuid;
                this.avaamo.send(bot_message_formatted);
            }
        }
    }).catch(function(e) {
        console.error("POST Catch Error", e);
    });
};

ChatBot.prototype.setGreetingMessage = function(botmessage) {
    let greetingNode = new Node({}, (context) => {
        botmessage.context = context;
        console.log("getFormattedMessage", botmessage.getFormattedMessage());
        return botmessage;
    }, 0);
    this.dialog = greetingNode;
};

ChatBot.prototype.getConversationTree = function(){
    let conversationTree = this.dialog.conversationDataTree();
    conversationTree.settings = {
        "agent": this.live_agent
    };
    return conversationTree;
};

ChatBot.prototype.run = function() {
    validate(this.dialog);

    Helper.POST({
        host: Helper.APP_SERVER_HOST,
        path: Helper.DASHBOARD_SERVER_CHATBOT_URI,
        port: Helper.APP_SERVER_PORT,
        method: "POST",
        headers: {
            "ACCESS-TOKEN": this.bot_access_token,
            "Content-Type": "application/json"
        }
    }, {
        "bot": {
            "conversation_datum": {
                "conversation_json": this.getConversationTree()
            },
            "enabled_for_dashboard_user": false,
            "bot_default_response": this.bot_default_response || [],
            "bot_greeting_response": null,
            "language_packs": null
        }
    }).then((response) => {
        JSON.stringify(response);
    }).catch((e) => {
        console.error(e);
    });
};

ChatBot.prototype.setDefaultMessages = function(messages) {
    this.bot_default_response = messages;
};

function processIncomingMessage(message, dialogObj) {
    console.log("Incoming Message:::", message, dialogObj);
    return new Promise((resolve) => {
        dialogObj.contains((node) => {
            if ((node.key === parseInt(message.current_step)) ||
                (message.current_step === "main" && node.key === 1)) {
                resolve(node.data.callbackFn.call(node, Context(message.context)));
            }
        });
    });
}

function ChatBotFactory(bot_uuid, bot_access_token) {
    if (!bot_uuid) {
        throw "bot_uuid cannot be empty";
    }
    if (!bot_access_token) {
        throw "bot_access_token cannot be empty";
    }
    return new ChatBot(bot_uuid, bot_access_token);
}

function validate(dialog) {
    if (!dialog)
        throw "Dialog cannot be empty";

}
module.exports = {
    ChatBot: ChatBotFactory,
    BotMessage: BotMessage,
    Node: Node,
    Intent: Intent
};
