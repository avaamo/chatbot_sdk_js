"use strict";
const fs = require("fs");
const UUID = require("uuid");
const FormData = require("form-data");
const Promise = require("promise");

const Credentials = {
    env: "prod",
    access_token: "",
    bot_uuid: "",
    device_uuid: UUID.v4()
};

const Logger = {
        debug: false,
        log() {
            if (this.debug === true) {
                for (let x = 0; x < arguments.length; x++) {
                    console.log(arguments[x], "\n");
                }
            }
            return this;
        }
    },
    UrlConfig = {
        dev: {
            app_server: "localhost",
            ds_server: "ws://localhost:4000",
            dashboard_server: "localhost",
            app_server_port: 3000,
            namespace: "",
            protocol: "http"
        },
        stag: {
            app_server: "staging.avaamo.com",
            ds_server: "wss://staging-ds.avaamo.com",
            dashboard_server: "staging-dashboard.avaamo.com",
            app_server_port: 443,
            namespace: "/s",
            protocol: "https"
        },
        prod: {
            app_server: "prod.avaamo.com",
            ds_server: "wss://ds.avaamo.com",
            dashboard_server: "dashboard.avaamo.com",
            app_server_port: 443,
            namespace: "/s",
            protocol: "https"
        }
    },
    env = global.ENV || "prod",
    http = require(UrlConfig[env].protocol);

const nameSpace = UrlConfig[env].namespace,
    Helper = {
        message_types: {
            MESSAGE_CONTENT_TYPE_IMAGE: "image",
            MESSAGE_CONTENT_TYPE_FILE: "file",
            MESSAGE_CONTENT_TYPE_PHOTO: "photo",
            MESSAGE_CONTENT_TYPE_AUDIO: "audio",
            MESSAGE_CONTENT_TYPE_VIDEO: "video",
            MESSAGE_CONTENT_TYPE_DEFAULT_CARD: "default_card",
            MESSAGE_CONTENT_TYPE_SMART_CARD: "smart_card",
            MESSAGE_CONTENT_TYPE_LINK: "link",
            MESSAGE_CONTENT_TYPE_RICHTEXT: "richtext",
            MESSAGE_CONTENT_TYPE_TEXT: "text",
            MESSAGE_CONTENT_TYPE_CARD_CAROUSEL: "card_carousel",
            MESSAGE_CONTENT_TYPE_FORM_RESPONSE: "form_response",
            MESSAGE_CONTENT_TYPE_CARD_RESPONSE: "card_response"
        },
        DS_HOST: UrlConfig[env].ds_server,
        DS_URI: (access_token, admin) => {
            return `/socket/websocket?access_token=${access_token}&user_agent=bot_server${admin ? "&admin=true" : ""}`;
        },
        APP_SERVER_HOST: UrlConfig[env].app_server,
        APP_SERVER_URI: `${nameSpace}/v1/messages.json`,
        APP_SERVER_MESSAGE_FETCH_URI: (uuid) => `${nameSpace}/messages/${uuid}.json`,
        APP_SERVER_FILES_URI: `${nameSpace}/files.json`,
        APP_SERVER_READ_ACK: `${nameSpace}/messages/read_ack.json`,
        APP_SERVER_PORT: UrlConfig[env].app_server_port,
        getFileAPI: (uuid) => `${nameSpace}/files/${uuid}.json`,
        getFormResponseAPI: (uuid) => `${nameSpace}/form/responses/${uuid}.json`,
        APP_SERVER_CONVERSATION_URI: `${nameSpace}/conversations/one_one_conversation.json`,
        APP_SERVER_SIGN_IN_URI: `${nameSpace}/embed/sign_in.json`,
        DASHBOARD_SERVER_HOST: UrlConfig[env].dashboard_server,
        DASHBOARD_SERVER_CHATBOT_URI: `${nameSpace}/bots.json`,

        isObject(item) {
            return (typeof item === "object" && !Array.isArray(item) && item !== null);
        },
        GET(url, accessToken) {
            let option = {
                hostname: this.APP_SERVER_HOST,
                port: this.APP_SERVER_PORT,
                path: url,
                method: "GET",
                headers: {
                    "Access-Token": accessToken || Credentials.access_token
                }
            };
            return new Promise(function(resolve, reject) {
                http.request(option, function(res) {
                    if (res.statusCode === 200) {
                        res.setEncoding("utf8");
                        let output = "";
                        res.on("data", function(data) {
                            output += data;
                        });
                        res.on("end", function() {
                            resolve(JSON.parse(output));
                        });
                    } else {
                        if (res.statusCode === 302 && res.headers.location) {
                            Helper.GET(res.headers.location);
                        } else {
                            reject(res.statusCode);
                        }
                    }
                }).on("error", function(e) {
                    reject(e);
                }).end();
            });
        },
        download(url, path, permission, name, accessToken) {
            path = path || __dirname;
            permission = permission || 466;
            name = (name || UUID.v4());
            let option = {
                    hostname: this.APP_SERVER_HOST,
                    port: this.APP_SERVER_PORT,
                    path: url,
                    method: "GET",
                    headers: {
                        "Access-Token": accessToken || Credentials.access_token
                    }
                },
                fileWrite = (path, permission, res, resolve) => {
                    try {
                        if (!fs.existsSync(path)) {
                            fs.mkdirSync(path, permission);
                        }
                        res.pipe(fs.createWriteStream(path + "/" + name));
                    } catch (e) {
                        console.error(e);
                    }
                    resolve(name);
                };

            return new Promise(function(resolve, reject) {
                http.request(option, function(res) {
                    if (res.statusCode == 200) {
                        fileWrite(path, permission, res, resolve);
                    } else {
                        if (res.statusCode === 302 && res.headers.location) {
                            http.get(res.headers.location, (file) => {
                                fileWrite(path, permission, file, resolve);
                            });
                        } else {
                            reject(res.statusCode);
                        }
                    }
                })
                .on("error", function(e) {
                    reject(e);
                }).end();
            });
        },
        POST(postOptions, data) {
            console.log(JSON.stringify(data));
            let promise = new Promise(function(resolve, reject) {
                let postData = JSON.stringify(data),
                    postReq = http.request(postOptions, function(res) {
                        Logger.log("Request sent to the server", `===> Status code ::: ${res.statusCode} ::: ${postOptions.path}`);
                        if (res.statusCode < 200 || res.statusCode > 201) {
                            Logger.log(`Request Data :: ${postData}`);
                            reject(res.statusMessage);
                            return;
                        }
                        res.setEncoding("utf8");
                        var data = "";
                        res.on("data", function(chunk) {
                            data += chunk;
                            Logger.log(chunk);
                        });
                        res.on("end", function() {
                            resolve(data);
                        });
                    });
                postReq.on("error", function(error) {
                    Logger.log("POST ON Error", error.statusMessage);
                    reject(error.statusMessage);
                });
                //postReq.write(postData);
                postReq.end(postData);
            });
            promise.catch(function(e) {
                console.error("POST Catch Error", e);
            });
            return promise;
        }
    };

const sendJSON = function sendJSON(data, access_token, url) {
    let promise = new Promise(function(resolve, reject) {
        let postData = JSON.stringify(data),
            postOptions = {
                host: Helper.APP_SERVER_HOST,
                path: (url || Helper.APP_SERVER_URI),
                port: Helper.APP_SERVER_PORT,
                method: "POST",
                headers: {
                    "device-id": Credentials.device_uuid,
                    "Access-Token": access_token,
                    "Content-Type": "application/json"
                }
            },
            postReq = http.request(postOptions, function(res) {
                Logger.log("Request sent to the server", `===> Status code ::: ${res.statusCode} ::: ${postOptions.path}`);
                if (res.statusCode < 200 || res.statusCode > 201) {
                    Logger.log(`Request Data :: ${postData}`);
                    reject(res.statusMessage);
                    return;
                }
                res.setEncoding("utf8");
                var data = "";
                res.on("data", function(chunk) {
                    data += chunk;
                    Logger.log(chunk);
                });
                res.on("end", function() {
                    resolve(data);
                });
            });
        postReq.on("error", function(error) {
            Logger.log("SendJSON ON Error", error.statusMessage);
            reject(error.statusMessage);
        });
        //postReq.write(postData);
        postReq.end(postData);
    });
    promise.catch(function(e) {
        console.error("SendJSON Catch Error", e);
    });
    return promise;
};
const getShowCaseImage = function(showcase_image_path, access_token) {
    return new Promise(function(resolve, reject) {
        if (showcase_image_path) {
            let data = {
                "data": fs.createReadStream(showcase_image_path)
            };
            console.log("Sending showcase image...");
            sendAttachment(data, access_token, Helper.APP_SERVER_FILES_URI).then(function(res) {
                resolve(res.file.uuid);
            }, function(err) {
                reject(err);
            });
        } else {
            reject("Error please specify showcase image path");
        }
    });
};
const sendAttachment = function sendAttachment(data, access_token, url) {
    return new Promise(function(resolve, reject) {
        let form = new FormData();
        for (let field in data) {
            form.append(field, data[field]);
        }
        form.submit({
            host: Helper.APP_SERVER_HOST,
            path: (url || Helper.APP_SERVER_URI),
            port: Helper.APP_SERVER_PORT,
            protocol: "https:",
            method: "POST",
            headers: {
                "Access-Token": access_token
            }
        }, function(err, res) {
            if (!err) {
                if (res.statusCode === 201) {
                    res.setEncoding("utf8");
                    var output = "";
                    res.on("data", function(data) {
                        output += data;
                    });
                    res.on("end", function() {
                        Logger.log("Attachment posted successfully");
                        resolve(JSON.parse(output));
                    });
                } else {
                    reject(res.statusMessage);
                    Logger.log("Attachment Posting Status code is not 201", res.statusCode);
                }
            } else {
                Logger.log(`Attachment Posting Error:: ${err.statusCode} :: ${err.statusMessage}`);
                reject(err.statusMessage);
            }
        });
    });
};

module.exports = {
    Logger,
    Helper,
    sendJSON,
    sendAttachment,
    Credentials,
    getShowCaseImage
};
