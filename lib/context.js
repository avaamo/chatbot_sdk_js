"use strict";
const _ = require("lodash");

function Context(context) {
    this.context = context || {};
    this.collectedVariables = {};
}

Context.prototype.user = function(){
    return this.context.user;
};

Context.prototype.conversation = function(){
    return {uuid:this.context.conversation_uuid};
};

Context.prototype.getUpdatedContext = function(){
    return this.collectedVariables;
};

Context.prototype.set = function(key, value) {
    console.log("Setting ", key, value);
    if (!_.isString(key)) {
        throw "Key should be string";
    }
    if (_.isFunction(value)) {
        throw "Value should not be of type function";
    }

    (this.context.variables)?(this.context.variables[key] = value):{};
    this.collectedVariables[key] = value;

};

Context.prototype.get = function(key) {
    return this.context.variables[key] || this.context[key];
};

function ContextFactory(context) {

    return new Context(context);
}
module.exports = ContextFactory;
