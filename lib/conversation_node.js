"use strict";
const Queue = require("./queue");
let _ref = 0;

function ConversationData(intent_data, callback) {

    this.intent_data = intent_data;
    this.callbackFn = callback;
}

function ConversationNode(data) {

    this.data = data;
    this.parent = null;
    this.children = [];
}

function traverseDF(node, callback) {

    // this is a recurse and immediately-invoking function

    (function recurse(currentNode) {
        // step 2
        for (var i = 0, length = currentNode.children.length; i < length; i++) {
            // step 3
            recurse(currentNode.children[i]);
        }

        // step 4
        callback(currentNode);

        // step 1
    })(node);

}

function traverseBF(node, callback) {

    var queue = new Queue();
    queue.enqueue(node);

    var currentDialog = queue.dequeue();

    while (currentDialog) {
        for (var i = 0, length = currentDialog.children.length; i < length; i++) {
            queue.enqueue(currentDialog.children[i]);
        }

        callback(currentDialog);
        currentDialog = queue.dequeue();
    }
}

ConversationNode.prototype.contains = function(callback, traversal) {
    if (!traversal) {
        traversal = traverseDF;
    }
    traversal.call(this, this, callback);
};

ConversationNode.prototype.goToNode = function(node) {
    this.goto = node.key;
};

ConversationNode.prototype.goToUserInputNode = function(node) {
    this.goto_on_input = node.key;
};

ConversationNode.prototype.toJSON = function() {

    if (!this.key) {
        throw "Key cannnot be null";
    }
    return {
        "intentData": this.data.intent_data || {},
        "goto": this.goto || "",
        "goto_on_input": this.goto_on_input || "",
        "key": this.key,
        "messages": [{
            "content_type": "native_code"
        }]
    };
};

ConversationNode.prototype.addNode = function(childNode) {

    let child = childNode,
        parent = this;
    if (parent) {
        parent.children.push(child);
        child.parent = parent;
    } else {
        throw new Error("Cannot add node to a non-existent parent.");
    }
};

ConversationNode.prototype.conversationDataTree = function() {
    let flow = {
        nodes: [],
        links: []
    };
    traverseBF(this, function(node) {

        flow.nodes.push(node.toJSON());
        if (node.parent) {
            flow.links.push({
                from: node.parent.key,
                to: node.key
            });
        }
    });
    return flow;
};

function ConversationNodeFactory(intent_data, callback, ref) {

    if (!intent_data) {
        throw "Intent cannot be empty";
    }
    if (!callback) {
        throw "Bot response callback cannot be empty";
    }
    if (ref === 0) {
        _ref = ref;
    }
    let conversationData = new ConversationData(intent_data, callback);
    let convNode = new ConversationNode(conversationData);
    _ref++;
    convNode.key = _ref;
    return convNode;
}

module.exports = ConversationNodeFactory;
