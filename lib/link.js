"use strict";

module.exports = {
    LINK_TYPE_WEBPAGE: "web_page",
    LINK_TYPE_POSTMESSAGE: "message",
    getWebpageLink: function(title, url) {
        if (!title) {
            throw "Title cannot be empty";
        }
        if (!url) {
            throw "URL cannot be empty";
        }
        return {
            title: title,
            type: this.LINK_TYPE_WEBPAGE,
            value: url
        };
    },

    getPostMessageLink: function(title, payload) {
        if (!payload) {
            throw "payload cannot be empty";
        }
        if (!title) {
            throw "Title cannot be empty";
        }
        return {
            title: title,
            type: this.LINK_TYPE_POSTMESSAGE,
            value: payload
        };
    }
};
