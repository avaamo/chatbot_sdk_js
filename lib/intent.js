"use strict";
module.exports = {

    getPhraseIntent(name, phrases) {
        return {
            "name": name,
            "keywords": phrases,
            "intent_type": "training_data"
        };
    },
    getStartsWithIntent(name, starts_with) {
        return {
            "name": name,
            "logic": {
                "starts_with": starts_with,
                "intent_type": "training_data"
            }
        };
    },
    getEqualsToIntent(name, equals_to) {
        return {
            "name": name,
            "logic": {
                "equals_to": equals_to,
                "intent_type": "training_data"
            }
        };
    },
    getContainsIntent(name, contains) {
        return {
            "name": name,
            "logic": {
                "contains": contains,
                "intent_type": "training_data"
            }
        };
    },
    getEntityPresenceIntent(name, domain, entity) {
        return {
            "name": name,
            "domain_key":domain,
            "entity_key": entity,
            "intent_type": "entity"
        };
    }
};
