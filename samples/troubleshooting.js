"use strict";

(function() {
    global.ENV = process.argv[2] || "stag";
    const ChatBotLib = require("./../lib/chatbot"),
        ChatBot = ChatBotLib.ChatBot,
        BotMessage = ChatBotLib.BotMessage,
        Node = ChatBotLib.Node,
        Intent = ChatBotLib.Intent,
        Link = require("./../lib/link");

    let bot = ChatBot("a0912eeb-48db-4777-9b1e-a7d1f9a22ad3", "X0C8ap9KqpnIJXC4zS2KO3fx4IEBhUAc");
    let greetingMessage = BotMessage();
    greetingMessage.addTextResponse("Welcome ${first_name} - I can help you troubleshoot the service problem. What service do you need help with?");
    greetingMessage.addCardResponse({
        "links": [Link.getPostMessageLink("Internet", "Internet"), Link.getPostMessageLink("Wifi", "Wifi"), Link.getPostMessageLink("Email", "Email")]
    });
    bot.setGreetingMessage(greetingMessage);
    let dialog = bot.dialog;

    let internetProblem = Node(Intent.getPhraseIntent("User has internet problem", ["Internet", "web"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("What type of connection do you have?");
        botMessage.addCardResponse({
            "links": [Link.getPostMessageLink("ADSL - Internet via my home line", "ADSL"), Link.getPostMessageLink("Cable - Internet via dedicated cable", "Cable"), Link.getPostMessageLink("NBN - Internet via National Broadband Network", "NBN")]
        });
        return botMessage;
    });
    dialog.addNode(internetProblem);

    let wifiProblem = Node(Intent.getPhraseIntent("User has wifi/email problem", ["Wifi", "email"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Got it. You need help with ${context.text}. Sorry, I have been trained to help you with Internet Connection troubleshooting only for now.");
        botMessage.addCardResponse({
            "links": [Link.getPostMessageLink("Internet Problems?", "Internet"), Link.getPostMessageLink("Talk to Agent", "#transfer to agent")]
        });
        return botMessage;
    });
    dialog.addNode(wifiProblem);

    let internetProblems = Node(Intent.getPhraseIntent("User has internet problem", ["Internet"]), () => {});
    wifiProblem.addNode(internetProblems);
    internetProblems.goToNode(internetProblem);

    let ADSLProblem = Node(Intent.getPhraseIntent("User has ADSL", ["ADSL"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("On the device having an internet connection issue, bring up your preferred web browser load up http://www.google.com/ Is that connecting?");
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Yes, it is connecting", "yes"),
                Link.getPostMessageLink("Yes, I can load the website, but it loads slowly", "Yes, I can load the website, but it loads slowly"),
                Link.getPostMessageLink("Yes, the website loads but my connection is intermittent", "Yes, the website loads but my connection is intermittent"),
                Link.getPostMessageLink("No, I can't load the website", "No")
            ]
        });
        return botMessage;
    });
    internetProblem.addNode(ADSLProblem);


    let cableProblem = Node(Intent.getPhraseIntent("User has Cable", ["Cable"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Got it. You need help with cable. Sorry, I have been trained to help you with ADSL only for now.");
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Try ADSL", "ADSL"),
                Link.getPostMessageLink("Talk to agent", "#transfer to agent")
            ]
        });
        return botMessage;
    });
    internetProblem.addNode(cableProblem);

    let tryADSL = Node(Intent.getPhraseIntent("User has internet problem", ["ADSL"]), () => {});
    cableProblem.addNode(tryADSL);
    tryADSL.goToNode(ADSLProblem);

    let nbnProblem = Node(Intent.getPhraseIntent("User has NBN", ["NBN"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Got it. You need help with NBN. Sorry, I have been trained to help you with ADSL only for now.");
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Try ADSL", "ADSL"),
                Link.getPostMessageLink("Talk to agent", "#transfer to agent")
            ]
        });
        return botMessage;
    });
    internetProblem.addNode(nbnProblem);

    let checkIfGoogleConnects = Node(Intent.getPhraseIntent("Check if User is able to access Google", ["yes", "Yes, I can load the website, but it loads slowly", "Yes, the website loads but my connection is intermittent", "No"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Can you now try to load up http://www.avaamo.com? Is that connecting?");
        botMessage.addCardResponse({
            title: "Is that connecting?",
            links: [
                Link.getPostMessageLink("Yes", "yes"),
                Link.getPostMessageLink("No", "No")
            ]
        });
        return botMessage;
    });
    ADSLProblem.addNode(checkIfGoogleConnects);

    let googleConnects = Node(Intent.getPhraseIntent("User is able to access Google", ["yes"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("That indicates there is no problem with your internet connection. \nMost likely the website your entered does not exist. Check the URL and try again.\nIf the problem persists contact the owner of the website.");
        botMessage.addCardResponse({
            title: "Did it address the problem?",
            links: [
                Link.getPostMessageLink("Yes", "yes"),
                Link.getPostMessageLink("No", "No")
            ]
        });
        return botMessage;
    });
    checkIfGoogleConnects.addNode(googleConnects);

    let troubleshootingSuccess = Node(Intent.getPhraseIntent("User is able to access Google", ["yes"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Hope you enjoyed the conversation.");
        return botMessage;
    });
    googleConnects.addNode(troubleshootingSuccess);

    let troubleshootingFail = Node(Intent.getPhraseIntent("Can't troubleshoot anymore", ["no"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("You got me there. I am sorry I have not been trained to debug this problem further. Would you like to chat with to our agents to help you further?");
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Connect to Live Agent", "#transfer to agent"),
                Link.getPostMessageLink("Troubleshoot a different issue", "Troubleshoot a different issue")
            ]
        });
        return botMessage;
    });
    googleConnects.addNode(troubleshootingFail);

    let troubleshootingDifferent = Node(Intent.getPhraseIntent("Restart troubleshooting", ["Troubleshoot a different issue"]), () => {});
    troubleshootingDifferent.goToNode(dialog);
    troubleshootingFail.addNode(troubleshootingDifferent);

    let couldNotConnectToGoogle = Node(Intent.getPhraseIntent("User not able to access google.", ["No"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Let me check if there is any outage in your area ...");
        botMessage.addTextResponse("There is no outage in your area. Now let us now check your modem.");
        botMessage.addTextResponse("I see that you have a Thompson TG 782T Modem.");
        botMessage.addTextResponse("What is the status of ECO/Power light?");
        botMessage.addCardResponse({
            title: "You can locate this light at the bottom right as highlighted here",
            links: [
                Link.getPostMessageLink("Off", "Off"),
                Link.getPostMessageLink("Solid Red", "Red"),
                Link.getPostMessageLink("Solid Green", "Green")
            ]
        });
        return botMessage;
    });
    checkIfGoogleConnects.addNode(couldNotConnectToGoogle);

    let modemIsOff = Node(Intent.getPhraseIntent("Modem power light is off", ["off"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Got it. The light is off");
        botMessage.addTextResponse("Check to make sure your modem's power cable is not the problem.");
        botMessage.addTextResponse("1. Plug the AC adapter cable securely into the back of the modem and to a working power point.\n2. Check if there is any damage to the cable or the adapter.\n3. Based on your modem (Thomson TG782T) make sure the adapter is 22V AC");
        botMessage.addCardResponse({
            title: "Did it address the problem?",
            links: [
                Link.getPostMessageLink("Yes", "Yes"),
                Link.getPostMessageLink("No", "No")
            ]
        });
        return botMessage;
    });
    couldNotConnectToGoogle.addNode(modemIsOff);
    modemIsOff.goToUserInputNode(googleConnects);

    let modemIsOn = Node(Intent.getPhraseIntent("Modem power light is ON", ["Red", "Green"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse(`Got it. The light is ${context.text}. :)`);
        botMessage.addTextResponse("What is the status of modem Internet light?");
        botMessage.addCardResponse({
            title: "You can locate this light at the top right as highlighted here",
            links: [
                Link.getPostMessageLink("Off", "Off"),
                Link.getPostMessageLink("Flashing Red", "Red"),
                Link.getPostMessageLink("Solid Green", "Green")
            ]
        });
        return botMessage;
    });
    couldNotConnectToGoogle.addNode(modemIsOn);

    let modemInternetStatus = Node(Intent.getPhraseIntent("Modem internet light status", ["Red", "Green", "Off"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse(`Got it. The light is ${context.text}. :)`);
        botMessage.addTextResponse("What is the status of modem WAN light?");
        botMessage.addCardResponse({
            title: "You can locate this light at the top right as highlighted here",
            links: [
                Link.getPostMessageLink("Off", "Off"),
                Link.getPostMessageLink("Red", "Red"),
                Link.getPostMessageLink("Green", "Green")
            ]
        });
        return botMessage;
    });
    modemIsOn.addNode(modemInternetStatus);

    let modemWANStatus = Node(Intent.getPhraseIntent("Modem WAN Light status", ["Red", "Green", "Off"]), (context) => {
        let botMessage = BotMessage(context);
        if (context.power_light_color === "off" &&
            context.modem_wan_light === "off" &&
            context.modem_internet_light === "off") {
            botMessage.addTextResponse(`The problem could be either the power cable is damaged or the power adapter has no reliable power source.\nThis problem cannot be resolved using a troubleshooting guide.`);
        } else if (context.power_light_color === "red" &&
            context.modem_wan_light === "red" &&
            context.modem_internet_light === "red") {
            botMessage.addTextResponse(`The problem is you have a faulty line filter.`);
        } else {
            botMessage.addTextResponse(`Thanks for all this information. We are now redirecting you to an agent with all the information you have provided to help you debug this further.`);
        }
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Continue Debugging with Agent", "#transfer to agent"),
                Link.getPostMessageLink("Troubleshoot a different issue", "Troubleshoot a different issue")
            ]
        });
        return botMessage;
    });
    modemInternetStatus.addNode(modemWANStatus);


    let troubleshootDiffIssue = Node(Intent.getPhraseIntent("Troubleshoot a different issue", ["Troubleshoot a different issue"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addCardResponse({
            links: [
                Link.getPostMessageLink("Internet", "Internet"),
                Link.getPostMessageLink("Wifi", "Wifi"),
                Link.getPostMessageLink("Email", "Email")
            ]
        });
        return botMessage;
    });
    modemWANStatus.addNode(troubleshootDiffIssue);
    troubleshootDiffIssue.goToUserInputNode(dialog);

    bot.setDefaultMessages(["Please check the input.", "I did not understand"]);
    bot.live_agent.emails = ["shanthivardhan@gmail.com"];
    bot.run();

})();
