"use strict";

(function() {
    global.ENV = process.argv[2] || "stag";
    const ChatBotLib = require("./../lib/chatbot"),
        ChatBot = ChatBotLib.ChatBot,
        BotMessage = ChatBotLib.BotMessage,
        Node = ChatBotLib.Node,
        Intent = ChatBotLib.Intent;

    let bot = ChatBot("d7e17d08-d5b5-4357-82ee-bfef9359c17d", "t-AarQKuEbLJRObBu5BsQqq_yWbQrKng");
    let greetingMessage = BotMessage();
    greetingMessage.addTextResponse("Hi, I am Shawn!  I can help you order a pizza.");
    greetingMessage.addTextResponse("Do you wan to order Pizza?");
    bot.setGreetingMessage(greetingMessage);
    let dialog = bot.dialog;

    let userWantsPizzaNode = Node(Intent.getPhraseIntent("User wants to order pizza", ["yeah", "yes"]), (context) => {

        let botMessage = BotMessage(context);
        botMessage.addTextResponse("We have 4 sizes available. small, medium, large and extra large. What size would you like?");
        return botMessage;
    });

    dialog.addNode(userWantsPizzaNode);

    let userDoesNotWantPizzaNode = Node(Intent.getPhraseIntent("User does not want to order pizza", ["No", "Nope", "nah"]), (context) => {

        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Okay, thank you for your order! Just let me know if you want to place another order.");
        return botMessage;
    });

    dialog.addNode(userDoesNotWantPizzaNode);

    let getSizeNode = Node(Intent.getEntityPresenceIntent("Get Pizza Size..", "pizza", "size"), (context) => {
        let botMessage = BotMessage(context);
        context.set("size", {"text":context.get("text")});
        botMessage.addTextResponse("We have a great selection of toppins.\n Cheese, pepperoni, garlic, chicken etc., What toppings are you in the mood for? (Limit 4)");
        return botMessage;
    });

    userWantsPizzaNode.addNode(getSizeNode);

    let getToppingsNode = Node(Intent.getPhraseIntent("Get Toppings", ["cheese", "pepperoni", "olives", "chicken"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Do you prefer pickup or delivery?");
        context.set("toppings", context.get("text"));
        return botMessage;
    });

    getSizeNode.addNode(getToppingsNode);

    let getDeliveryTypeNode = Node(Intent.getPhraseIntent("Get delivery type", ["pickup", "delivery", "togo"]), (context) => {
        context.set("delivery_option", context.get("text"));
        let botMessage = BotMessage(context);
        botMessage.addTextResponse(`Ok, I have one ${context.get("size").text} pizza with ${context.get("toppings")} for ${context.get("delivery_option")}. Is that correct?`);
        return botMessage;
    });
    getToppingsNode.addNode(getDeliveryTypeNode);

    let confirmDeliveryNode = Node(Intent.getPhraseIntent("All info is correct", ["yes", "yeah"]), (context) => {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse(`Thank you. The pizza will be ready by 20 mins. Do you want to place another order?`);
        botMessage.addCardResponse({"title":"Sample Card Title", "description":"Sample card description"});
        return botMessage;
    });
    confirmDeliveryNode.goToNode(userWantsPizzaNode);
    getDeliveryTypeNode.addNode(confirmDeliveryNode);
    bot.setDefaultMessages(["Hey I am a demo bot! I have not been trained to respond to this message currently."]);
    bot.live_agent.emails = ["shanthivardhan@gmail.com"];
    bot.run();

})();
