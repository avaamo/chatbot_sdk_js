"use strict";
const _ = require("lodash"),
    date = function () {
        return (new Date()).toISOString().replace("T", " ").replace("Z", "");
    };

let log = console.log,
    warn = console.warn,
    info = console.info,
    error = console.error,
    debug = console.info;

/*******************************************
 * NEVER ADD console.fn statements in this *
 *******************************************/

const Logger = {
    postLog(statement) {},
    getLogStmt(level, args) {
        let items = [].slice.call(args),
            meta = `[${date()}] [${level}]`,
            logStmt =  items.map(item => {
                if(_.isObject(item)) {
                    return JSON.stringify(item);
                }
                return item;
            });
        logStmt.unshift(meta);
        return logStmt.join(" ");
    },
    logFn(logFn, level, args) {
        let stmt = this.getLogStmt(level || "info ", args);
        (logFn || log).call(console, stmt);
        this.postLog(stmt);
    },
    log() {
        this.logFn(log, "info ", arguments);
    },
    debug() {
        this.logFn(debug, "debug", arguments);
    },
    info() {
        this.logFn(info, "info ", arguments);
    },
    warn() {
        this.logFn(warn, "warn ", arguments);
    },
    error() {
        this.logFn(error, "error", arguments);
    }
};
console.log = Logger.info.bind(Logger);
console.error = Logger.error.bind(Logger);
console.info = Logger.info.bind(Logger);
console.warn = Logger.warn.bind(Logger);
console.log = Logger.info.bind(Logger);
console.debug = Logger.debug.bind(Logger);

module.exports = Logger;
