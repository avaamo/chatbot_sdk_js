# Avaamo Node.js ChatBot SDK

Avaamo ChatBot Node.js SDK helps in implementing custom ChatBots using Avaamo Platform.

To implement a custom chatbots, you need to have [Avaamo Premium] account.

Once you are familiar with Avaamo Premium Dashboard, [create a bot from the dashboard and grab the bot-uuid and access-token by following the steps mentioned in the wiki].

This repository will help you to implement a custom chatbot with samples written in JavaScript.

The repository has `lib` and `assets` directory which you need not worry about. It contains the SDK code implementation and supporting assets for the sample we had built.

The `samples/pizza.js` file is the sample bot application which has the example code.

## Pre-requisite

Node.js version >= 4.2.4 NPM version >= 2.14.12 Novice level Server-Side JavaScript knowledge would be good enough.

To give a bit of an overview about the bot implementation, bots are treated as another user in the Avaamo platform. Once you create a bot from the dashboard, it will appear in the Avaamo messaging application for your group of users or your company members. Any user from your group/company can send message to the bot. How will the bot respond to the messages sent to it? This repository is built to answer this question.

Any messages sent to the bot can be intercepted and replied more sensibly to solve your business needs. The implementation is a 4 step process:

## Step 1: Including the SDK file

To use the SDK, you have to copy the files in the `lib` directory into one of your implementation directories and require the SDK file `chatbot.js` as given below:

```
require('./lib/chatbot')
```
`chatbot.js` is a Utility module that provides functions to create a ChatBot and its required components ConversationNode, Intent and BotMessage.

**NOTE: The SDK has few NPM dependencies. Run `npm install` at the where `package.json` file is present.**

## Step 2: Instantiating `ChatBot` class

Once you require `chatbot.js` you can access ChatBot factory function to instantiate the ChatBot, please check the following snippet:

```
const ChatBotLib = require("./lib/chatbot"),
    ChatBot = ChatBotLib.ChatBot;

let bot = ChatBot(bot_uuid, access_token);
```
Once you are done with creating a chatbot object set a greeting message to it. To set a greeting message you would need to in


```JavaScript
(function myChatBot() {
  let bot_uuid = "<bot-uuid>",
    access_token = "<bot-access-token>";
    const ChatBotLib = require("./lib/chatbot"),
        ChatBot = ChatBotLib.ChatBot,
        BotMessage = ChatBotLib.BotMessage,
        Node = ChatBotLib.Node,
        Intent = ChatBotLib.Intent;

    let bot = ChatBot(bot_uuid, access_token);
    let greetingMessage = BotMessage();
    greetingMessage.addTextResponse("Hi, I am Shawn!  I can help you order a pizza.");
    greetingMessage.addTextResponse("Do you wan to order Pizza?");
    bot.setGreetingMessage(greetingMessage);
    let dialog = bot.dialog;

})();
```

## Step 3: Building the bot definition and flow for the bot interaction
In a typical conversation, the bot takes the user through a series of questions to achieve a goal. It can be capturing all the options for a pizza/sandwich order or information necessary to debug a support ticket related to an Internet modem.
In this example, this is how we create the first set of nodes and later add them to the chat bot.
```JavaScript
    let userWantsPizzaNode = Node(Intent.getPhraseIntent("User wants to order pizza", ["yeah", "yes"]), function intentMatchCallback(context) {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("We have 4 sizes available. small, medium, large and extra large. What size would you like?");
        return botMessage;
    });
    dialog.addNode(userWantsPizzaNode);
```

And here is what should happen when the user responds with a "no".
```JavaScript
    let userDoesNotWantPizzaNode = Node(Intent.getPhraseIntent("User does not want to order pizza", ["No", "Nope", "nah"]), function intentMatchCallback(context) {
        let botMessage = BotMessage(context);
        botMessage.addTextResponse("Okay, thank you for your order! Just let me know if you want to place another order.");
        return botMessage;
    });
    dialog.addNode(userDoesNotWantPizzaNode);
```
Working with Context variables

You can set and retrieve some information that you might require at a later stage by using the context.
Setting variables:
```JavaScript
    function intentMatchCallback(context) {
        context.set("size", "small");
    }
```
Once you have set a variable to the context, it will always be available with all the incoming messages unless the flow is reset.
Retrieving the context variables:
```JavaScript
function intentMatchCallback(context) {
    context.get("size");
}
```
[avaamo premium]: http://www.avaamo.com/premium.html
[click here to see the full code sample]: ./samples/pizza.js
[create a bot from the dashboard and grab the bot-uuid and access-token by following the steps mentioned in the wiki]: https://github.com/avaamo/java/wiki
